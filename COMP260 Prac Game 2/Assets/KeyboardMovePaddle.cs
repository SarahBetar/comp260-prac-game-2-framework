﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class KeyboardMovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;
	public float force = 10f;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}

	void Update() {
	}

	void FixedUpdate () {
		Vector3 dir;
		dir.x = Input.GetAxis ("Horizontal");
		dir.y = Input.GetAxis ("Vertical");
		dir.z = 0;

		rigidbody.AddForce(dir * force);
	}

}
