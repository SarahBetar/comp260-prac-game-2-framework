﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PaddleAI : MonoBehaviour {

	private Rigidbody rigidbody;
	public float speed = 10f;
	public float rand;
	public Transform puck;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {
		float calc = puck.position.z - rigidbody.position.z;
		Vector3 dir = new Vector3 (0, 0, calc);

		Vector3 vel = dir.normalized * speed;

		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;
	}
}
