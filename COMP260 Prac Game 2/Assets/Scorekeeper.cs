﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour {

	static private Scorekeeper instance;
	public MovePaddle paddle;
	public int pointsPerGoal = 1;
	public Text[] scoreText;
	public Text winText;
	private int[] score = new int[2];

	static public Scorekeeper Instance {
		get { return instance; }
	}

	void Start () {
		if (instance == null) {
			// save this instance
			instance = this;
		} else {
			// more than one instance exists
			Debug.LogError(
				"More than one Scorekeeper exists in the scene.");
		}

		//reset the scores to zero
		for (int i = 0; i < score.Length; i++) {
			score [i] = 0;
			scoreText [i].text = "0";
		}
	}    

	public void OnScoreGoal(int player) {
		score[player] += pointsPerGoal;
		scoreText[player].text = score[player].ToString();
		if (score [player] >= 10) {
			winText.text = "Player " + player.ToString() + " wins!";
			paddle.speed = 0f;
		}
	}

	void Update() {
	}
}

